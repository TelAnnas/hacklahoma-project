#Json file maker
import json
import os

def json_maker(response_text):


    try:
        response_json = json.loads(response_text)
        # Write the JSON data to a file
        with open("response.json", "w") as f:
            json.dump(response_json, f)
            f.flush()
            os.fsync(f.fileno())
    except json.JSONDecodeError:
        print("Error decoding JSON data")
    
