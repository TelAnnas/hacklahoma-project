import openai
import json
import os
import config 
from Json_maker import json_maker 

# Define OpenAI API key 
openai.api_key = config.chatGPT_apikey

# Set up the model and prompt
model_engine = "text-davinci-003"

def analyze_text(text):

    prompt = f"Analyze the following article:\n{text}\nand export a json file with the following variables filled out (headline:, date:,time:,latitude:,longitude:,summary:,URL: ) - estimate the longitude and latitude based on the streets and addresses referenced. Make the summary a 2 sentence blurb about suspect details"

    # Generate a response
    completion = openai.Completion.create(
        engine=model_engine,
        prompt=prompt,
        max_tokens=200,
        n=1,
        stop=None,
        temperature=0.5,
    )

    #print(completion.choices[0].text)

    response_text = completion.choices[0].text
    #print(response_text)

    json_maker(response_text)



