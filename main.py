from flask import Flask, request, render_template, jsonify
from twilio import twiml
import math
import os
import json
from twilio.twiml.messaging_response import MessagingResponse, Message
from twilio.rest import Client
import keys
app = Flask(__name__)

@app.route('/')
def home():
    return render_template('home.html')

user_latitude = 0.0
user_longitude = 0.0
@app.route('/test', methods=['POST'])
def test():
    output = request.get_json()
    result = json.loads(output) #this converts the json output to a python dictionary
    user_latitude = result['latitude']
    user_longitude = result['longitude']
    return result

# Load the JSON file
with open('response.json') as f1, open('response2.json') as f2:
    data1 = json.load(f1)
    data2 = json.load(f2)


# Get the longitude and latitude values
latitude_target = data1['latitude']
longitude_target = data1['longitude']
latitude_target2 = data2['latitude']
longitude_target2 = data2['longitude']

#Get the headlines
headlines1 = data1['headline']
headline2 = data2['headline']

#Get the dates
dates1 = data1['date']
dates2 = data2['date']

#Get the times
time1 = data1['time']
time2 = data2['time']

#Gettting URL
URL1 = data1['URL']
URL2 = data2['URL']

def is_within_radius(user_latitude, user_longitude, target_lat, target_lng):
    R = 3963.1676  # Earth's radius in miles
    lat1 = math.radians(user_latitude)
    lat2 = math.radians(float(latitude_target))
    lat_diff = math.radians(float(latitude_target) - user_latitude)
    lng_diff = math.radians(float(longitude_target) - user_longitude)
  
    a = (
        math.sin(lat_diff / 2) ** 2 +
        math.cos(lat1) * math.cos(lat2) * math.sin(lng_diff / 2) ** 2
    )
    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a))
    distance = R * c
  
    if distance <= 5:
        print('You are within 5 miles of the target location!')
        
    else:
        print('You are more than 5 miles away from the target location.')
  
    return distance <= 5  # Return True if distance is less than or equal to 5 miles

        
client= Client(keys.account_sid, keys.auth_token) 
# Message content
headline = headlines1  # replace with the name of your headline variable
date = dates1  # replace with the name of your date variable
time = time1  # replace with the name of your time variable
url_link = URL1 # replace with the name of your URL variable
message_body = f"{headline}\n\n{date} at {time}\n\n"
message = client.messages.create(
body = message_body,
from_ = keys.twilio_number,
to = keys.my_phone_number
)

if __name__ == "__main__":
    is_within_radius(user_latitude, user_longitude, latitude_target, longitude_target)
    app.run()