import os
import sys
from twilio.rest import Client
import keys
from ..main import headlines1, dates1, time1, URL1
#headlines1, headline2, dates1, dates2, time1, time2, URL1, URL2

client= Client(keys.account_sid, keys.auth_token) 
# Message content
headline = headlines1  # replace with the name of your headline variable
date = dates1  # replace with the name of your date variable
time = time1  # replace with the name of your time variable
url_link = URL1 # replace with the name of your URL variable
message_body = f"{headline}\n\n{date} at {time}\n\n{url_link}"

message = client.messages.create(
    body = message_body,
    from_ = keys.twilio_number,
    to = keys.my_phone_number
)

print(f"Message SID: {message.sid}")